package ukm;

public class main {

    public static void main(String[] args) {
        System.out.println("============================================");
        System.out.println("============ Menghitung Iuran ==============");
        System.out.println("============================================");
        ukm u = new ukm("UKM Futsal Fst");

        mahasiswa maha = new mahasiswa();
        maha.setnama("Mario");
        maha.setnim("195314130");
        maha.settempatTgllahir("Kefamenanu, 08 September 2001");
        u.setketua(maha);
         
        mahasiswa sekre = new mahasiswa("19531413", "Rikar", "Kupang, 31 agustus 2001");
        u.setsekretaris(sekre);
        penduduk[] pend = new penduduk[3];
        mahasiswa[] m = new mahasiswa[2];
        m[0] = new mahasiswa();
        m[0].setnama("Kevin");
        m[0].setnim("195314150");
        m[0].settempatTgllahir("Maumere, 30 juli 2003");
        pend[0] = m[0];

        m[1] = new mahasiswa();
        m[1].setnama("Gandi");
        m[1].setnim("195314153");
        m[1].settempatTgllahir("Bijaepasu, 20 November 2001");
        pend[1] = m[1];

        masyarakatSekitar[] mas = new masyarakatSekitar[1];
        mas[0] = new masyarakatSekitar();
        mas[0].setnama("Putri");
        mas[0].setnomor("3452");
        mas[0].settempatTgllahir("Bali, 01 Januari 2000");
        pend[2] = mas[0];

        System.out.println("");
        System.out.println("==============="+u.getnamaUnit()+"===============");
        System.out.println("");
        double tot = 0;
        double iuranMaha = 0, iuranMasya = 0;
        for (int i = 0; i < pend.length; i++) {
            System.out.println("ANGGOTA KE " + (i + 1) + "");
            System.out.println("nama : " + pend[i].getnama());
            System.out.println("Tempat Tanggal Lahir : " + pend[i].gettempatTgllahir());
            if (pend[i] instanceof mahasiswa) {
                mahasiswa mahas = (mahasiswa) pend[i];
                System.out.println("Nim : " + mahas.getnim());
                System.out.println("Iuran : " + mahas.hitungIuran());
                iuranMaha = +mahas.hitungIuran();
            } else if (pend[i] instanceof masyarakatSekitar) {
                masyarakatSekitar masy = (masyarakatSekitar) pend[i];
                System.out.println("Nomor : " + masy.getnomor());
                System.out.println("Iuran : " + masy.hitungIuran());
                iuranMasya = +masy.hitungIuran();
            }
            System.out.println("");
            System.out.println("============================================");
        }
tot = iuranMaha + iuranMasya;
        System.out.println("Total Iuran : "+tot);
    }

}

